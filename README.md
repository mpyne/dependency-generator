This tool parses .kde-ci.yml files from a checkout of repositories and spits out dependency metadata as expected by kdesrc-build

# Building

`cargo build`

# Run

```
export REPOS=/home/nico/kde/src
export REPO_METADATA=/home/nico/kde/src/sysadmin-repo-metadata

cargo run
```

The result is printed on stdout.

# Arguments

Use `--list` to get a list of all relevant projects.

Use `--qt6` to generate dependencies for a Qt6/KF6 build. By default Qt5 is used.
