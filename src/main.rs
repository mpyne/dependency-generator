use clap::Parser;
use serde::Deserialize;
use serde_yaml::Value;
use std::collections::BTreeMap as Map;
use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use walkdir::WalkDir;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    list: bool,
    #[arg(long)]
    qt6: bool,
}

#[derive(PartialEq, Clone, Copy)]
enum QtVersion {
    Qt5,
    Qt6,
}

#[derive(Deserialize)]
struct MetaData {
    repopath: String,
    projectpath: String,
    repoactive: bool,
}

#[derive(Deserialize)]
pub struct KdeCi {
    #[serde(rename = "Dependencies")]
    pub dependencies: Option<Vec<Dependencies>>,
}

#[derive(Deserialize)]
pub struct Dependencies {
    pub on: Vec<String>,
    pub require: Require,
}

#[derive(Deserialize)]
pub struct Require {
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn list_dependencies(full_name: &str, qt: QtVersion) -> Vec<String> {
    let base_name = full_name.split("/").into_iter().nth(1).unwrap().to_string();

    let kdeci_file = std::fs::File::open(format!(
        "{}/{}/.kde-ci.yml",
        &env::var("REPOS").unwrap(),
        base_name
    ));

    if kdeci_file.is_err() {
        return vec![];
    }

    let kdeci: KdeCi = serde_yaml::from_reader(kdeci_file.unwrap()).unwrap();

    let dependencies = kdeci.dependencies.unwrap_or_default();

    let mut result: Vec<String> = vec![];

    for dep in &dependencies {
        let should_include: bool = (|| {
            if dep.on.contains(&String::from("@all")) {
                return true;
            }

            if dep.on.contains(&String::from("Linux")) {
                return true;
            }

            if qt == QtVersion::Qt5 && dep.on.contains(&String::from("Linux/Qt5")) {
                return true;
            }

            if qt == QtVersion::Qt6 && dep.on.contains(&String::from("Linux/Qt6")) {
                return true;
            }

            return false;
        })();

        if !should_include {
            continue;
        }

        for (key, _) in &dep.require.other {
            result.push(key.to_string());
        }
    }

    return result;
}

fn get_repo_metadata() -> Map<String, MetaData> {
    let mut result = Map::new();

    for entry in WalkDir::new(format!(
        "{}/projects-invent",
        &env::var("REPO_METADATA").unwrap()
    ))
    .into_iter()
    .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();

        if f_name.ends_with(".yaml") {
            if entry.path().to_str().unwrap().contains("websites") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("wikitolearn") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("webapps") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("historical") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("documentation") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("sysadmin/") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("neon") {
                continue;
            }

            if entry.path().to_str().unwrap().contains("packaging") {
                continue;
            }

            let md: MetaData =
                serde_yaml::from_reader(std::fs::File::open(entry.path()).unwrap()).unwrap();

            let key = &md.repopath;

            if !md.repoactive {
                continue;
            }

            result.insert(key.to_string(), md);
        }
    }

    return result;
}

fn main() {
    let metadata = get_repo_metadata();

    let all_projects: Vec<String> = metadata.iter().map(|(key, _)| key.to_string()).collect();

    let args = Args::parse();

    if args.list {
        for project in all_projects {
            println!("{}", project);
        }

        return;
    }

    let qt6_ignored_projects = vec![
        "frameworks/khtml",
        "frameworks/kdelibs4support",
        "frameworks/kdewebkit",
        "frameworks/kemoticons",
        "frameworks/kinit",
        "frameworks/kjs",
        "frameworks/kjsembed",
        "frameworks/kmediaplayer",
        "frameworks/kross",
        "frameworks/kdesignerplugin",
        "frameworks/kxmlrpcclient",
        "libraries/kross-interpreters",
        "libraries/kwebkitpart",
    ];

    println!("# This file has been auto-generated from .kde-ci.yml files using https://invent.kde.org/nicolasfella/dependency-generator");
    println!("# Instead of editing it manually re-run the generation");

    for project in &all_projects {
        let qt = match args.qt6 {
            true => QtVersion::Qt6,
            false => QtVersion::Qt5,
        };

        if qt == QtVersion::Qt6 && qt6_ignored_projects.contains(&project.as_str()) {
            continue;
        }

        let deps = list_dependencies(project, qt);

        println!();
        println!("# {}", project);

        for dep in &deps {
            let projectpath;

            if qt == QtVersion::Qt6 && qt6_ignored_projects.contains(&dep.as_str()) {
                continue;
            }

            if dep.contains("third-party") {
                projectpath = dep;
            } else {
                projectpath = &metadata[dep].projectpath;
            }

            println!("{}: {}", &metadata[project].projectpath, projectpath)
        }
    }
}
